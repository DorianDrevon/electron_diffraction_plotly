import dash, dash_core_components as dcc, dash_html_components as html, dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
from dorianUtils.utilsD import Utils
from dorianUtils.dccExtendedD import DccExtended
import dash_daq as daq
import numpy as np, pandas as pd
import plotly.express as px, plotly.graph_objects as go
import tifffile, cbf
import os, base64,re

import glob
from dorianUtils.utilsD import Callback
from multislice import pets as pt
from plotly.subplots import make_subplots
from zipfile import ZipFile


callbackUtils = Callback()
utils=Utils()
dccE=DccExtended()

app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP],title='cool la diffraction')

class ProcessingData():
    def __init__(self,folderData,molecule_init='glycine'):
        self.methods = [
            'processed',
            'Bloch',
            'kinematics',
            'excitation error',
            'potential',
            'lattice',
            'multi slice',
            ]
        self.folderMolecules = folderData
        self.getMolecules_list()
        self.initMolecule    = molecule_init
        self.real_eds        = self.load_ED(self.initMolecule)
        self.df_simulation   = self.load_simulated_data(self.initMolecule)

    def getMolecules_list(self):
        self.molecules = [k.split('/')[-1] for k in glob.glob(self.folderMolecules+'*')]

    def load_ED(self,molecule):
        listTiffs = glob.glob(self.folderMolecules + molecule + '/tiffs/*')
        listTiffs.sort()
        real_eds=[]
        for f in listTiffs:
            # print(f)
            real_eds.append(tifffile.imread(f))
        real_eds = np.array(real_eds)
        return real_eds

    def load_ED_frame(self,molecule,frame):
        listTiffs = glob.glob(self.folderMolecules + molecule + '/tiffs/*')
        f = [k for k in listTiffs if int(re.findall('\d{5}',k)[0])==frame]
        if len(f)>0:
            return tifffile.imread(f)
        else:
            return []

    def load_simulated_data(self,molecule,ext='rpl'):
        ptsFiles = glob.glob(self.folderMolecules + molecule + '/pets/*.pts')
        pets = pt.Pets(ptsFiles[0],gen=0)
        if ext=='rpl':
            df_pets = pets.rpl
        elif ext=='xyz':
            rpl_hkl = pets.invA.dot(pets.xyz[['x','y','z']].values.T)
            pets.xyz[['h','k','l']] = np.array(np.round(rpl_hkl),dtype=int).T
            df_pets=pets.xyz
        return df_pets

    def load_cbf(self,frame):
        tifffile.imread(frame)
        content = cbf.read(frame)
        numpy_array_with_data = content.data
        header_metadata = content.metadata
        return numpy_array_with_data

    def computeTF(self,methods,frameNb,df_simulation=None):
        if df_simulation is None:
            df_simulation=self.df_simulation
        dfs =[]
        colors=utils.getColorHexSeq(len(methods),'jet')
        for method,symbol,color in zip(methods,utils.raw_symbols,colors):
            if method=='processed':
                # dfCalcul = self.df_simulation[self.df_simulation.F==frameNb][['rpx','rpy','I','h','k','l']]
                dfCalcul = df_simulation[df_simulation.F==frameNb][['px','py','I','h','k','l']]
            else:
                dfCalcul=pd.DataFrame(np.random.randint(0,520,[2,100]).T,columns=['px','py'])
                dfCalcul['I'] = 3000*np.random.randn(len(dfCalcul))
                dfCalcul.I = dfCalcul.I.abs()
            dfCalcul['method']=method
            dfs.append(dfCalcul)
        return pd.concat(dfs,axis=0)

    def plot_TED(self,methods,frameNb,maxZ,real_eds=None,df_simulation=None,superimposed=False,flipud=True):
        if real_eds is None : real_eds=self.real_eds
        simulated_frame = cfg.computeTF(methods,frameNb,df_simulation)
        #### get miller indices
        millerIndices = ['(' + ','.join([str(k) for k in np.random.randint(0,20,3)]) + ')' for l in range(len(simulated_frame))]

        fig = px.scatter(simulated_frame,x='px',y='py',size='I',color='method',symbol='method')
        if superimposed:
            colCompute=1
        else:
            colCompute=2
        figTotal = make_subplots(rows=1, cols=colCompute)
        hovertemplatemiller='x : %{x} <br> y : %{y} <br> intensity : %{marker.size:.2f} <br> miller:%{text}'
        for trace in fig.data:
            figTotal.add_trace(trace,row=1,col=colCompute)

        real_ED = real_eds[frameNb-1]
        if flipud:
            real_ED = np.flipud(real_ED)
        realed_trace = go.Heatmap(z=real_ED,colorscale='gray',zmin=0,zmax=maxZ,colorbar_x=-0.25)
        figTotal.add_trace(realed_trace,col=1,row=1)
        figTotal.update_traces(text=millerIndices,hovertemplate=hovertemplatemiller)
        # print(figTotal)
        shapeEDS = real_eds.shape
        figTotal.update_xaxes(range=[0,shapeEDS[1]])
        figTotal.update_yaxes(range=[0,shapeEDS[2]])
        aspectRatio,h = shapeEDS[1]/shapeEDS[2],700
        # print(figTotal)
        figTotal.update_layout(height=h,width=h*aspectRatio*colCompute)
        return figTotal

    def plot_TED_frame(self,methods,frameNb,molecule,maxZ,df_simulation=None,superimposed=False,flipud=True):
        ##=============== simulated frame================
        simulated_frame = cfg.computeTF(methods,frameNb,df_simulation)
        #### get miller indices
        millerIndices = ['(' + ','.join([str(k) for k in np.random.randint(0,20,3)]) + ')' for l in range(len(simulated_frame))]

        fig = px.scatter(simulated_frame,x='px',y='py',size='I',color='method',symbol='method')
        if superimposed:
            colCompute=1
        else:
            colCompute=2
        figTotal = make_subplots(rows=1, cols=colCompute)
        hovertemplatemiller='x : %{x} <br> y : %{y} <br> intensity : %{marker.size:.2f} <br> miller:%{text}'
        for trace in fig.data:
            figTotal.add_trace(trace,row=1,col=colCompute)

        ##=============== real ED frame================
        real_ED_frame = self.load_ED_frame(molecule,frameNb)
        if flipud:
            real_ED_frame = np.flipud(real_ED_frame)
        realed_trace = go.Heatmap(z=real_ED_frame,colorscale='gray',zmin=0,zmax=maxZ,colorbar_x=-0.25)
        figTotal.add_trace(realed_trace,col=1,row=1)
        figTotal.update_traces(text=millerIndices,hovertemplate=hovertemplatemiller)
        # print(figTotal)
        shapeEDS = real_ED_frame.shape
        figTotal.update_xaxes(range=[0,shapeEDS[0]])
        figTotal.update_yaxes(range=[0,shapeEDS[1]])
        aspectRatio,h = shapeEDS[0]/shapeEDS[1],700
        # print(figTotal)
        figTotal.update_layout(height=h,width=h*aspectRatio*colCompute)
        figTotal.update_layout(title={
                    'text':molecule + 'my friend, frame : ' + str(frameNb),
                    'x':0.5,'xanchor':'center'})

        return figTotal

class TabED():
    def __init__(self,app,cfg):
        self.app = app
        self.cfg = cfg

        dd_method = [
            html.P('select your method :'),
            dcc.Dropdown(id='dd_method',
                    options=[{'value':t,'label':t} for t in self.cfg.methods],
                    value=['processed'],multi=True,clearable=True)]

        dd_molecule = [
            html.P('select your molecule :'),
            dcc.Dropdown(id='dd_molecule',
                    options=[{'value':t,'label':t} for t in self.cfg.molecules],
                    value=self.cfg.initMolecule,multi=False,clearable=False)]

        store_data = dcc.Store(id='st_data',storage_type='memory')
        # store_data = dcc.Store(id='st_data')

        uploadDcc = dcc.Upload(
            id='up_folder',
            multiple=False,
            children=html.Div([
                'upload your tiffs(format .zip)'
            ]),
            style={
                'width': '95%', 'height': '60px', 'lineHeight': '60px',
                'borderWidth': '1px', 'borderStyle': 'dashed',
                'borderRadius': '5px', 'textAlign': 'center', 'margin': '10px'
            },
        )
        inputZmax = [
            html.P('select the maximum of the colorscale'),
            dcc.Input(id='in_zmax',type='number',value=100)
            ]

        tsSuper=daq.ToggleSwitch(id='toogle_Superposition',value=True,label='superimposed',color='blue')
        tsFlip=daq.ToggleSwitch(id='toogle_flip',value=False,label='flip_up down',color='green')

        widgetLayout = [uploadDcc]+ dd_molecule + dd_method + inputZmax + [dbc.Row([dbc.Col(tsSuper),dbc.Col(tsFlip)])]+[store_data]

        config={
                'displaylogo': False
                }
        fig = go.Figure()
        widthG = 85
        dccGraph = dcc.Graph(id='edDiff_graph',config = config,figure=fig)
        maxFrame = self.cfg.real_eds.shape[0]
        zSlider = dcc.Slider(
            id='frameSlider',
            min=1,
            marks={int(k):str(int(k)) for k in np.linspace(0,maxFrame,20)},
            max=maxFrame,
            step=1,
            value=5
            )
        graphLayout=[html.Div([dccGraph,zSlider],style={"width": str(widthG)+"%", "display": "inline-block"})]

        self.tabLayout = [html.Div(widgetLayout,style={"width": str(100-widthG) + "%", "float": "left"})]+graphLayout
        self.tabname = 'premier onglet amigo'

    def save_file(self,name, content):
        """Decode and store a file uploaded with Plotly Dash."""
        data = content.encode("utf8").split(b";base64,")[1]
        filezip = os.path.join(self.cfg.folderMolecules, name)
        with open(filezip, "wb") as fp:
            fp.write(base64.decodebytes(data))
        """unzip the file """
        molecule = name.split('.zip')
        if len(molecule)==0:
            print('your file is not a .zip TOCAR')
            return 0
        molecule=molecule[0]
        fullpathMol = self.cfg.folderMolecules + molecule
        with ZipFile(filezip, 'r') as zipObj:
           zipObj.extractall(fullpathMol)
        # if os.listdir(fullpathMol)~=['tiffs', 'pets']:
        os.remove(filezip)

    def _define_callbacks(self):
        @self.app.callback(
            Output('dd_molecule', 'options'),
            Output('dd_molecule', 'value'),
            Input('up_folder', 'filename'),
            State('up_folder', 'contents'),
            prevent_initial_call=True
            )
        def upload_molecule(filename,contents):
            """Save uploaded files and regenerate the file list."""
            if filename is not None and contents is not None:
                res=self.save_file(filename, contents)
            if res==0:
                return
            ## options of dd molecule
            molecule = filename.split('.zip')[0]
            self.cfg.getMolecules_list()
            new_options=[{'value':t,'label':t} for t in self.cfg.molecules]
            return new_options,molecule
            # return new_options

        @self.app.callback(
            Output('st_data', 'data'),
            Output('frameSlider', 'max'),
            Output('frameSlider', 'marks'),
            Input('dd_molecule', 'value'),
            )
        def update_molecule(molecule):
            simulated_df = self.cfg.load_simulated_data(molecule)
            # update slider
            listTiffs = glob.glob(self.cfg.folderMolecules + molecule + '/tiffs/*')
            maxFrame = max([int(re.findall('\d{5}',k)[0]) for k in listTiffs])
            newMarks = {int(k):str(int(k)) for k in np.linspace(0,maxFrame,20)}
            df_simulation = simulated_df.to_dict('records')
            # print(maxFrame,newMarks)
            return df_simulation,maxFrame,newMarks

        @self.app.callback(
            Output('edDiff_graph', 'figure'),
            Input('dd_method','value'),
            Input('frameSlider', 'value'),
            Input('in_zmax', 'value'),
            Input('toogle_Superposition', 'value'),
            Input('toogle_flip', 'value'),
            Input('dd_molecule', 'value'),
            Input('st_data', 'data'),
            prevent_initial_call=True
            )
        def updateGraph(methods,frameNb,maxZ,superimposed,flip,molecule,df_simulation):
            df_simulation = pd.DataFrame(df_simulation)
            # fig = self.cfg.plot_TED(methods,frameNb,maxZ,real_eds,df_simulation,superimposed,flipud=True)
            fig = self.cfg.plot_TED_frame(methods,frameNb,molecule,maxZ,df_simulation,superimposed,flipud=flip)
            fig.update_layout(title={
                        'text':molecule + '  my friend, frame : ' + str(frameNb),
                        'x':0.5,'xanchor':'center'})
            return fig

##############################
#   CHANGE HERE THE FOLDER   #
# WHERE YOU WANT TO PUT YOUR #
#       DATA FOLDER          #
##############################
folderData  = '/home/dorian/.venv/edly/molecules/'

##### INITIALIZATION OF INSTANCES
cfg = ProcessingData(folderData)
tabEd = TabED(app,cfg)
tabEd._define_callbacks()
tabs = [tabEd]
tabsLayout= dccE.createTabs(tabs)


### add modals
mdFile   = 'logVersion.md'
titleLog = 'electron diffraction topissime'
modalLog = dccE.addModalLog(app,titleLog,mdFile)
# modalErrors = [m.modalError for m in tabs]
app.layout = html.Div([html.Div([modalLog]),html.Div(tabsLayout)])

app.run_server(host='0.0.0.0',debug=True,use_reloader=True)

# ===============================

def test():
    # methods = ['kinematics','processed','mes fesses','top']
    methods = ['processed','mes fesses']
    # methods = ['']
    mol = 'dummy_glycine'
    frame=5
    real_ed = cfg.load_ED_frame(mol,frame)
    # real_eds = cfg.load_ED(mol)
    df_simulated = cfg.load_simulated_data(mol)
    # fig=cfg.plot_TED(methods,frameNb=5,maxZ=50,real_eds=real_eds,df_simulation=df_simulated,superimposed=True,flipud=True)
    fig=cfg.plot_TED_frame(methods,5,mol,50,df_simulation=df_simulated,superimposed=True,flipud=True)
    fig.show()

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
name="edly",
version="4.0.5",
author="Dorian Drevon",
author_email="drevondorian@gmail.com",
description="visualisation tool to look at electron diffraction pattern",
long_description=long_description,
long_description_content_type="text/markdown",
# url="https://github.com/pypa/sampleproject",
# project_urls={
#     "Bug Tracker": "https://github.com/pypa/sampleproject/issues",
# },
classifiers=[
    "Programming Language :: Python :: 3",
    "License :: OSI Approved :: MIT License",
    "Operating System :: OS Independent",
],
# packages=setuptools.find_packages(),
install_requires=['dorianUtilsModulaire==3.13.8','pandas==1.3.1','dash==1.20.0',
                    'dash_daq==0.5.0','openpyxl==3.0.7','tifffile==2021.11.2','cbf==1.1.3',
                    'TarikDrevonUtils'],
python_requires=">=3.8"
)
